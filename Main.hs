{-# LANGUAGE OverloadedStrings, TypeApplications #-}
module Main where

import Data.Maybe
import Data.YAML (Node(..), Scalar(..), Pos, decode1)
import Options.Applicative

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Map             as Map
import qualified Data.Text            as T

-- * A few useful type synonyms

type EnvVar = String
type Command = String
type JobName = String
type ImageURL = String

type RepoUser = String
type RepoProj = String
type Ref = String

-- * Main functions

main :: IO ()
main = do
  args@(RunJob fp _ _ _ _) <- opts
  LBS.readFile fp >>= f args . fmap dropPositions . (decode1 @(Node Pos))

  where -- we don't care about the position of YAML entities in the
        -- gitlab CI file
        dropPositions = fmap (const ())

        -- handle YAML decoding error
        f _    (Left err)   = error (show err)
        f args (Right node) = go args (toCIScript node)

        -- generate the build and docker scripts for a given set of
        -- CLI args.
        go (RunJob _ jname repoUser repoProj ref) ciscript =
          case Map.lookup jname (jobs ciscript) of
            Nothing -> error $ "couldn't find job " ++ jname
            Just j  -> do
              let j' = finalJob jname j ciscript
              buildScriptFor jname j' repoUser repoProj ref ciscript
              dockerScriptFor jname j' ciscript

-- | Generate the docker script for a given job configuration.
--   We try to stop and remove a container with that name if it
--   already exists, then create a new one, copy the build script
--   there and finally run it, attaching its tty to the current
--   terminal.
dockerScriptFor :: JobName -> Job -> CIScript -> IO ()
dockerScriptFor jobname j ciscript =
  writeFile "docker.sh" (unlines shScript)

  where shScript =
          [ "#!/usr/bin/env sh"
          , ""
          , dockerStopCmd
          , dockerRmCmd
          , dockerCreateCmd jobname j ciscript
          , dockerCopyBuildCmd
          , dockerRunBuildCmd
          , dockerAttachCmd
          ]

        dockerStopCmd = unwords
          ["docker", "stop", jobname, "||", "true"]

        dockerRmCmd = unwords
          ["docker", "rm", "-fv", jobname, "||", "true"]

        dockerCopyBuildCmd = unwords
          ["docker", "cp", "./build.sh", jobname++":/home/ghc/build.sh"]

        dockerRunBuildCmd = unwords
          ["docker", "exec"
              , "-w", "/home/ghc"
              , "-it"
              , jobname
              , "sh", "./build.sh"
          ]

        dockerAttachCmd = unwords
          ["docker", "attach", "--sig-proxy", jobname]

-- | A simple wrapper for @docker run@ that gets the image from the
--   suitable url, taking care of substituting the @$DOCKER_REV@ environment
--   variable by the value it is given at the toplevel of the @.gitlab-ci.yml@
--   script specified with @-f@.
dockerCreateCmd :: JobName -> Job -> CIScript -> String
dockerCreateCmd jobname j ciscript = unwords
  ["docker", "run", "--name", jobname, "-d", "-t", subst dockerRev (image j)]

  where subst _ Nothing = error "no image for job!"
        subst r (Just img) = replaceDockerRev r img

        replaceDockerRev r ('$':'D':'O':'C':'K':'E':'R':'_':'R':'E':'V':s) = r ++ s
        replaceDockerRev r (x:xs) = x : replaceDockerRev r xs
        replaceDockerRev _ [] = []

        dockerRev = case lookup "DOCKER_REV" (globalEnvVars ciscript) of
          Nothing -> error "no DOCKER_REV variable found???"
          Just (SStr r)  -> T.unpack r
          Just _         -> error "DOCKER_REV had an unexpected type"


-- | Generate the build script for a given job configuration.
--
--   The generated script does the following:
--
--   - it exports all the relevant environment variables
--     (whether listed in the global @variables:@ section or in a job-specific
--     one);
--   - it clones GHC and checks out the appropriate git ref;
--   - it runs the (combined?) @before_script@s;
--   - it runs the (combined?) @script@s;
--   - it runs the (combined?) @after_script@s;
buildScriptFor
  :: JobName -> Job -> RepoUser -> RepoProj -> Ref -> CIScript
  -> IO ()
buildScriptFor _jname job repoUser repoProj ref ciscript =
  writeFile "build.sh" (unlines shScript)

  where shScript =
          [ "#!/usr/bin/env sh"
          , ""
          ] ++
          map exportVar (globalEnvVars ciscript) ++
          map exportVar (jobEnvVars job) ++
          concatMap ppScript [ ("git_script", gitScript)
                             , ("before_script", beforeScript job)
                             , ("script", script job)
                             , ("after_script", afterScript job)
                             ]

        gitScript =
          [ "git clone https://gitlab.haskell.org/ghc/ghc.git"
          , "cd ghc"
          ] ++
          [ "git remote add r " ++
            "https://gitlab.haskell.org/" ++ repoUser
                                   ++ "/" ++ repoProj
                                   ++ ".git ; " ++
            "git fetch r"
          | repoUser /= "ghc" && repoProj /= "ghc" ] ++
          [ "git checkout " ++ ref | ref /= "master" ]

-- * Agglomerating variables, scripts, ...

-- | Take the job with the given name and take its @extends:@
--   closure, merging variables, scripts and more until we've
--   got everything in a single 'Job' value, that we can later
--   pretty print to get a build script.
--
--   The job we're interested in is "merged" with its parents using
--   'mergeL': we keep the original data for all the fields that can
--   essentially only be set once (e.g @image: ...@).
finalJob :: JobName -> Job -> CIScript -> Job
finalJob name job ciscript = applyExtends job

  where applyExtends j = case extends j of
          Nothing -> j
          Just name' -> case Map.lookup name' (jobs ciscript) of
            Nothing -> error $
              name ++ " extends " ++ name' ++ " doesn't exist"
            Just j' -> applyExtends (mergeL name j name' j')


-- | Left-biased merging of jobs.
--
--   The new job's 'extends' field is set to the second job's value,
--   so as to later follow that link and process the last parent's own
--   parent, in 'finalJob'.
--
--   The @image@ field is not "merged": we make sure at most one of the two
--   images is set.
--
--   We merge dinstinct environment variables but error out if we end up trying
--   to set an env var twice.
--
--   The @before_script@s are merged by mere concatenation of the commands,
--   since our current CI script does specify two of those for at least one job.
--
--   The other scripts are treated like @image@: can only be set once.
mergeL :: String -> Job -> String -> Job -> Job
mergeL n1 j1 n2 j2 =
  Job (extends j2) (mergeImage j1 j2) (mergeVars j1 j2)
      (mergeScripts n1 (beforeScript j1) n2 (beforeScript j2) True)
      (mergeScripts n1 (script j1)       n2 (script j2)       False)
      (mergeScripts n1 (afterScript j1)  n2 (afterScript j2)  False)

-- | Make sure that at most one of the jobs has an 'image' defined, and
--   return it, when it is indeed defined in one of them.
mergeImage :: Job -> Job -> Maybe ImageURL
mergeImage j1 j2 = case (image j1, image j2) of
  (Just i1, Just i2) -> error $ "two images! " ++ show (i1, i2)
  (i1, Nothing) -> i1
  (Nothing, i2) -> i2

-- | Merge env vars. Error out when a given var is set twice.
mergeVars :: Job -> Job -> [(EnvVar, Scalar)]
mergeVars j1 j2
  | xs@(_:_) <- [ v | (v, _) <- jobEnvVars j1
                    , isJust (lookup v (jobEnvVars j2))
                ] ++
                [ v | (v, _) <- jobEnvVars j2
                    , isJust (lookup v (jobEnvVars j1))
                ]
    = error $ "duplicate variables in jobs: " ++ show xs
  | otherwise = jobEnvVars j1 ++ jobEnvVars j2

-- | Merge list of commands by concatenating them.
--
--   The 'Bool' argument controls whether we should error out in case both lists
--   are non-empty, or whether we should append the lists (in the opposite order
--   as the one they appear in in the list of arguments.
mergeScripts :: String -> [Command] -> String -> [Command] -> Bool -> [Command]
mergeScripts _ [] _ xs _ = xs
mergeScripts _ xs _ [] _ = xs
mergeScripts nx xs ny ys False = error . unlines $
  "both scripts are defined!" :
  show (nx, ny) :
  (xs ++ ["==="] ++ ys)
mergeScripts _ xs _ ys True = ys ++ xs

-- * CLI options

-- | A @ghc-repro@ command.
data Cmd = RunJob FilePath -- ^ path to the @.gitlab-ci.yml@ file
                  JobName  -- ^ name of the job in the CI script
                  RepoUser -- ^ gitlab.haskell.org user from which to get ghc
                  RepoProj -- ^ gitlab.haskell.org project name from which to get ghc
                  Ref      -- ^ git ref to checkout before building
  deriving (Eq, Show)

cmd :: Parser Cmd
cmd = RunJob
  <$> strOption
      ( long "file"
     <> short 'f'
     <> value "./.gitlab-ci.yml"
     <> help "Path to the .gitlab-ci.yml file to use"
     <> metavar "FILE"
      )
  <*> strOption
      ( long "job"
     <> short 'j'
     <> help "Job name (as written in .gitlab-ci.yml), e.g release-x86_64-linux-deb9-dwarf"
     <> metavar "JOBNAME"
      )
  <*> strOption
      ( long "user"
     <> short 'u'
     <> value "ghc"
     <> help "Gitlab user to which the GHC repository belongs"
     <> metavar "USER"
      )
  <*> strOption
      ( long "project"
     <> short 'p'
     <> value "ghc"
     <> help "Name of the Gitlab repository/project"
     <> metavar "REPONAME"
      )
  <*> strOption
      ( long "ref"
     <> short 'r'
     <> value "master"
     <> help "Git reference to checkout before starting the build"
     <> metavar "GIT_REF"
      )

opts :: IO Cmd
opts = execParser $ info (cmd <**> helper)
  ( fullDesc
 <> progDesc "Automate the execution of GHC jobs with Docker"
 <> header "ghc-repro - run CI jobs locally with Docker"
  )

-- * Representing a @.gitlab-ci.yml@ script

-- | Our representation of a gitlab CI script
data CIScript = CIScript
  { globalEnvVars :: [(EnvVar, Scalar)] -- ^ global @variables:@ section
  , globalBeforeScript :: [Command]     -- ^ global @before_script@ section
  , jobs :: Map.Map JobName Job         -- ^ all the jobs (including the "meta"
                                        --   ones, that don't get run but are
                                        --   only extended)
  } deriving (Eq, Show)

-- | Our representation of a job
data Job = Job
  { extends :: Maybe JobName         -- ^ @extends: ...@
  , image :: Maybe ImageURL          -- ^ @image: ...@
  , jobEnvVars :: [(EnvVar, Scalar)] -- ^ @variables: ...@
  , beforeScript :: [Command]        -- ^ @before_script: ...@
  , script :: [Command]              -- ^ @script: ...@
  , afterScript :: [Command]         -- ^ @after_script: ...@
  } deriving (Eq, Show)

-- | Lookup all the jobs in the given toplevel YAML mapping node
getJobs :: Map.Map (Node ()) (Node ()) -> Map.Map JobName Job
getJobs m = Map.fromList
  [ (ppStr k, getJob m')
  | (k, Mapping _ _ m') <- Map.toList m
  , isJobRelated (ppStr k)
  ]

-- | Read the given YAML mapping node as a 'Job'
getJob :: Map.Map (Node ()) (Node ()) -> Job
getJob m =
  let ext = ppStr <$> lookupKey "extends" m
      img = ppStr <$> lookupKey "image" m
      envvars = vars m
      bscript = getBeforeScript m
      mscript = getScript m
      ascript = getAfterScript m
  in Job ext img envvars bscript mscript ascript

-- | Read the given YAML mapping node as a 'CIScript'
getCIScript :: Map.Map (Node ()) (Node ()) -> CIScript
getCIScript m =
  let envvars = vars m
      bscript = getBeforeScript m
      js      = getJobs m
  in CIScript envvars bscript js

-- | Convert a toplevel YAML 'Node' to a 'CIScript'. The input must
--   be a 'Mapping'.
toCIScript :: Node () -> CIScript
toCIScript (Scalar _ _)     = error "Didn't expect a scalar at the top"
toCIScript (Sequence _ _ _) = error "Didn't expect a sequence at the top"
toCIScript (Anchor _ _ _)   = error "Didn't expect an anchor at the top"
toCIScript (Mapping _ _ m)  = getCIScript m

-- | Lookup the @variables@ key in the given mapping and extract its contents
--   as a list of @(env_var, value)@ pairs.
vars :: Map.Map (Node ()) (Node ()) -> [(String, Scalar)]
vars m = case lookupKey "variables" m of
  Just (Mapping _ _ vs) -> map simpleVar (Map.toList vs)
  _                     -> []

-- | Lookup the given key in the given YAML mapping node.
lookupKey :: String -> Map.Map (Node ()) (Node ()) -> Maybe (Node ())
lookupKey k m = Map.lookup (Scalar () (SStr (T.pack k))) m

getBeforeScript, getScript, getAfterScript
  :: Map.Map (Node ()) (Node ()) -> [Command]
getBeforeScript = lookupScript "before_script"
getScript = lookupScript "script"
getAfterScript = lookupScript "after_script"

-- | Lookup the list of commands under a field with the given label
--   (e.g @"before_script"@) in the mapping node given as argument.
lookupScript :: String -> Map.Map (Node ()) (Node ()) -> [Command]
lookupScript lbl m = case Map.lookup (Scalar () (SStr (T.pack lbl))) m of
  Just (Sequence _ _ s) -> map ppStr s
  _                     -> []

-- * Helpers for printing and extracting

ppStr :: Node () -> String
ppStr (Scalar _ (SStr s)) = T.unpack s
ppStr _ = error "ppStr: not a (scalar) string"

getScal :: Node () -> Scalar
getScal (Scalar _ s) = s
getScal _ = error "getScal: not a scalar"

isJobRelated :: String -> Bool
isJobRelated s = not $
  s `elem` ["stages", "variables", "before_script", ".only_default"]


simpleVar :: (Node (), Node ()) -> (String, Scalar)
simpleVar (scal, val) = (ppStr scal, getScal val)

exportVar :: (EnvVar, Scalar) -> String
exportVar (k, val) = "export " ++ k ++ "=" ++ val'
  where val' = case val of
          SNull -> "null"
          SBool b -> show b
          SInt i -> show i
          SFloat f -> show f
          SStr s -> show s
          SUnknown _ t -> T.unpack t ++ " (unknown)"

ppScript :: (String, [Command]) -> [String]
ppScript (_, []) = []
ppScript (lbl, cmds) =
  let hdr = "#### " ++ lbl ++ " ####" in
    (hdr : cmds) ++ [replicate (length hdr) '#']
